### Start array job and resume program when all jobs are completed

#### Install
```bash
git clone git@gitlab.inria.fr:gdesrues1/OarJobArrayExample.git
cd OarJobArrayExample

source /etc/profile.d/modules.sh
module load conda/2020.11-python3.8

python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
deactivate

export SIMUS="/data/epione/user/$USER/SIMUS"  # or in `~/.bash_profile`
./start.sh
```

#### Usage
The entry point is `start.sh`.
It will start in the backgound the python `main.py` program.
You can run `ps aux | grep main.py` to check the backgound process.

The logs of `main.py` are located in `logs`.
All scripts needed to run oar will be generated to the `generated` directory, 
OAR logs to `generated/logs`.

For example:
```bash
.
├── generated
│   ├── array_args.txt
│   ├── logs
│   │   ├── OAR.XXX.stderr
│   │   └── OAR.XXX.stdout
│   ├── notify_exec.sh
│   ├── oarsub_res.txt
│   ├── pipe_file.fifo
│   ├── runme.sh
│   └── start_oar.sh
├── job.py
├── logs
│   ├── main.stderr
│   ├── main.stdout
│   └── time.txt
├── main.py
└── start.sh
```